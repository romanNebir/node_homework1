const fs = require('fs');
const path = require('path');

const createFile = (req, res) => {
    const { filename, content } = req.body;
    const extensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
    if (!filename) {
        return res.status(400).json({ message: 'Please, specify filename' });
    };
    if (!extensions.includes(path.extname(filename))) {
        return res.status(400).json({ message: 'Sorry, file with such extension is not allowed' });
    }
    if (!content) {
        return res.status(400).json({ message: 'Please, specify content' });
    };

    const filePath = path.join(__dirname, '..', '/files', filename);

    const dirname = path.dirname(filePath);
    if (!fs.existsSync(dirname)) {
        fs.mkdirSync(dirname);
    }

    fs.writeFile(filePath, content, (err) => {
        if (err) {
            return res.status(500).json({ message: 'Server error' });
        };
        res.status(200).json({ message: 'File created successfully' });
    })

};

const getFiles = (req, res) => {
    const dirPath = path.join(__dirname, '..', '/files');
    fs.readdir(dirPath, (err, files) => {
        if (err) {
            return res.status(500).json({ message: 'Server error' });
        };
        res.status(200).json({ message: 'Success', files })
    })
};

const getFile = (req, res) => {
    const filename = req.params.filename;
    const filePath = path.join(__dirname, '..', '/files', filename);
    fs.readFile(filePath, 'utf-8', (err, content) => {
        if (err) {
            if (err.code === 'ENOENT') {
                return res.status(400).json({ message: `No file with ${filename} filename found` })
            }
            return res.status(500).json({ message: 'Server error' });
        }
        res.status(200).json({
            message: 'Success',
            filename,
            content,
            extension: path.extname(filename).substring(1),
            uploadedDate: fs.statSync(filePath).birthtime
        })
    })
};

module.exports = {
    createFile,
    getFiles,
    getFile
}
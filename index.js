const express = require('express');
const filesRouter = require('./routers/filesRouter');
const morgan = require('morgan');
const cors = require('cors');

const PORT = process.env.PORT || 8080;

const app = express();

app.use(cors());
app.use(express.json());

app.use(morgan('short'));

app.use('/api/files', filesRouter);

app.listen(PORT, () => {
    console.log(`Server has been started on port ${PORT}...`);
});
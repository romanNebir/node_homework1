const express = require('express');
const { createFile, getFiles, getFile } = require('../controllers/filesControllers')

const router = express.Router();

router.post('/', createFile);
router.get('/', getFiles);
router.get('/:filename', getFile);

module.exports = router;